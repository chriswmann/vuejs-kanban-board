# Kanban

> An AO kanban board in vue.js

> Based on the tutorial by Steve Hobbs at Auth0:
> https://auth0.com/blog/vuejs-kanban-board-the-development-process/

# Screenshots
### Add Items Screen
![Add Items Screen](https://bitbucket.org/chriswmann/vuejs-kanban-board/raw/efa739fb44e02d4942d0dc7f07dd1e901931beab/screenshots/kanban-board-add-items.png)
### Task Lane Screen
![Task Lane Screen](https://bitbucket.org/chriswmann/vuejs-kanban-board/raw/efa739fb44e02d4942d0dc7f07dd1e901931beab/screenshots/kanban-board-tasklanes.png)
### Draggable items (via vuedraggable)
![Item Being Dragged](https://bitbucket.org/chriswmann/vuejs-kanban-board/raw/efa739fb44e02d4942d0dc7f07dd1e901931beab/screenshots/kanban-board-with-vuedraggable.png)
## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
